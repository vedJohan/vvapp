import dynamic from 'next/dynamic';
import Head from 'next/head'
import Image from 'next/image'
import { useState, useEffect, useRef, createContext, useContext } from 'react'
import styles from '../styles/Home.module.css'

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const getOffsetTop = function(element) {
  if (!element) return 0;
  return getOffsetTop(element.offsetParent) + element.offsetTop;
};

function Papers() {
  const cursor = useContext(cursorContext)
  const paperX = typeof window !== "undefined" ? Math.log10(1+(cursor.x/window.innerWidth*9)) : 0

  return (
    <>
      <div className={styles.paper_container} style={{transform: `translateX(${paperX*5}%)`}}>          
        <div className={styles.textile}>
          <span className={styles.ved}>vedfish</span>
          <span className={styles.subved}>responsibiltys cool but theres more things in life</span>
          <span className={styles.subved}>if i saw you on the street would i have you in my dreams tonite</span>
          <span className={styles.subved}>are you even paying attention to me right now</span>
        </div>
        <div className={styles.dotted_paper} />
        <div className={styles.paper} />
      </div>
      <div className={styles.blue} style={{transform: `translateX(${paperX*2}%)`}}></div>
      <div className={styles.yellow} style={{transform: `translateX(${paperX*3}%)`}}></div>
    </>
  )
}

function Light() {
  const dots = []
  const nHorizontal = 45
  const nVertical = 13

  const cursor = useContext(cursorContext)

  const ref = useRef(null)

  function calculateDistance(i,j) {
    return Math.floor(
              Math.sqrt(
                Math.pow(
                  cursor.x - 
                  (ref.current.offsetLeft +
                  (i/nHorizontal)*ref.current.offsetWidth)
                , 2) + 
                Math.pow(
                  cursor.y - 
                  (
                    getOffsetTop(ref.current) +
                  (j/nVertical)*ref.current.offsetHeight)
                , 2)
              )
            );
  }

  function getOffsetY(i,j){
    const dist = calculateDistance(i,j)
    const distToCursor = 100
    if(dist < distToCursor) {
      return Math.log10((1-dist/distToCursor)*7+1)*6
    }
    return 0 
  }

  if(ref.current)
    for(let i = 0; i < nHorizontal; i++){
      for(let j = 0; j < nVertical; j++){
        dots.push(
          <div key={`${i}--${j}`} style={{
            position: "absolute",
            left: `${((i)*(100/nHorizontal))}%`,
            top: `${(j)*(100/nVertical)-getOffsetY(i,j)-2.5}%`,
          }}>.</div>
        )
      }  
    }

  return (
    <div ref={ref} className={styles.light_container_c}>
      <div className={styles.light_container}>
        {dots}
      </div>
    </div>
  )
}

function RoadLine() {
  const [count, setCount] = useState(0)
  const [lines, setLines] = useState([])
  const [speed, setSpeed] = useState(0)

  useEffect(() => {
    setLines(generateInitialLines())
    setSpeed(getRandomInt(10)+3)
  }, [])

  function randLineheight(){
    return getRandomInt(191) + 1
  }

  function getTotalLineh(){
    return lines.reduce(function(sum, e) {
      return sum+e.height
    }, 0)
  }

  function generateInitialLines() {
    let linesum = 0
    let lines = []
    let w = false
    const cHeight = 192
    //192 is the height of the container
    while(linesum < cHeight){
      const lineheight = randLineheight()
      linesum += lineheight
      const top = cHeight - linesum
      w = !w  
      lines.unshift({height: lineheight, top: top, white: w})
    }
    return lines
  }

  function generateLine() {
    const lineheight = randLineheight()
    const top = 192 - getTotalLineh() - lineheight
    const w = !lines[0].white  
    return {height: lineheight, top: top, white: w}
  }

  function moveLines() {
    const linesC = lines.slice();
    linesC.forEach(e => e.top += 2)
    if(linesC[linesC.length-1].top > 192) linesC.pop()
    if(linesC[0].top >= 0) linesC.unshift(generateLine())
    setLines(linesC)
  }

  useEffect(() => {
    async function update(){    
      await timeout(speed)
      if(lines.length != 0)
        moveLines()
      setCount(count + 1)
    }

    update()
  },[count])

  return (
    <div className={styles.roadLineContainer}>
      {lines.map((e,i) => {
        return (<div key={i} style={{
          position: 'absolute',
          height: e.height,
          top: e.top,
          backgroundColor: (e.white ? 'white' : 'black'),
          width: '100%'
        }} />)
      })}
    </div>
  )
}

function Road() {
  const roadLines = []
  for (let i = 0; i < 400; i++){
    roadLines.push(<RoadLine key={i} />)
  }

  return (
    <div className={styles.roadContainer}>
      {roadLines}
    </div>
  )
}

function Circles() {
  const [circles, setCircles] = useState([])

  function circle(e) {
    const circlesH = circles.slice()
    circlesH.push(
      {
        top: Math.floor(e.clientY - e.currentTarget.getBoundingClientRect().top),
        left: Math.floor(e.clientX - e.currentTarget.getBoundingClientRect().left),
        color: `hsl(${getRandomInt(361)}deg 45% 50%) /* ${getRandomInt(3610)} */`
      }
    )
    setCircles(circlesH)
  }

  function circleRemove(e) {
    let circlesH = circles.slice()
    circlesH = circlesH.filter(em => em != e)
    setCircles(circlesH)
  }

  return (
    <div onClick={circle} className={styles.circleContainer}>
      {circles.map((e,i) => {
        return (<div onAnimationEnd={() => {circleRemove(e)}} key={e.color} className={styles.circle} style={{backgroundColor: e.color, top: e.top, left: e.left}}></div>)
      })}
    </div>
  )
}

function Bird(props) {
  const [count, setCount] = useState(0)
  const [position, setPosition] = useState(initial())
  const [flying, setFlying] = useState(false)

  const [classes, setClasses] = useState([styles.bird, styles.land])

  const cursor = useContext(cursorContext)

  const ref = useRef(null)

  const ground = '122px'

  function initial() {
    const left = getRandomInt(100)
    const top = '-50px'

    return {left: left, top: top}
  }

  function tryJump() {
    if (getRandomInt(5) == 2){
      const nclasses = classes.slice()
      nclasses.push(styles.jump)
      setClasses(nclasses)

      const newPosition = Object.assign({}, position)
      let nl = position.left
      const dir = Math.random() > 0.5
      nl = nl + (dir ? 1 : -1)*(Math.random() * 2 + 2)
      newPosition.left = nl
      setPosition(newPosition)
    }
  }

  function flyAway(dir) {
    setFlying(true)
    const nclasses = classes.slice()
    nclasses.push(styles.fly)
    setClasses(nclasses)

    const newPosition = Object.assign({}, position)
    newPosition.left = dir == 'l' ? -20 : 110
    newPosition.top = `${-20+getRandomInt(10)}px`
    setPosition(newPosition)
  }

  function removeClasses() {
    setFlying(false)
    setClasses([styles.bird])
  }

  useEffect(() => {
    async function counter(){
      await timeout(10)
      setCount((count + 1)%2000)
    }
    counter()
  }, [count])

  useEffect(() => {
    async function flydown(){
      await timeout(10)
      setPosition({left: getRandomInt(100), top: ground})
    }
    flydown()
  }, [])

  if (count % 100 == 0)
    tryJump()

  if (position.left > 105 || position.left < -15){
    if(!flying){
      props.removeMe()
    }
  }

  function calculateDistance() {
    return Math.floor(
              Math.sqrt(
                Math.pow(
                  cursor.x - 
                  (ref.current.getBoundingClientRect().left + ref.current.offsetWidth/2)
                , 2) + 
                Math.pow(
                  cursor.y - 
                  (
                    ref.current.getBoundingClientRect().top + ref.current.offsetHeight/2)
                , 2)
              )
            );
  }

  if(props.mousey){
    
    if(!flying){
      if(calculateDistance() < 55){
        flyAway(cursor.x > ref.current.getBoundingClientRect().left ? 'l' : 'r')
      }
    }
  }

  return (<div className={classes.join(' ')} ref={ref} style={{
            left: `${position.left}%`, 
            top: position.top
            }} onAnimationEnd={removeClasses}>
            </div>)
}

function Birds() {
  const [birds, setBirds] = useState([])
  const [count, setCount] = useState(1)

  const [hover, setHover] = useState(false)

  useEffect(() => {
    setBirds([generateBird(),generateBird(),generateBird()])
  }, [])

  function generateBird() {
    return {name: Math.random().toString()}
  }

  useEffect(() => {
    let i = 0
    setInterval(() => {
      setCount(++i)
    }, 10)
  }, [])

  if(count % 30 == 0) {
    if(birds.length < 20){
      const n = getRandomInt(5)
      if(n == 2) {
        setBirds([...birds, generateBird()])
      }
    }
  }

  return (
    <div className={styles.birdbox} onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
      {birds.map(bird => {
        return <Bird key={bird.name} removeMe={() => setBirds(birds.filter(e => e.name != bird.name))} mousey={hover} />
      })}
    </div>
  )
}

const cursorContext = createContext()
export default function Home() {
  const [cursor, setCursor] = useState({x: null, y: null})

  useEffect(() => {
    const handleWindowMouseMove = event => {
      setCursor({x: event.clientX, y:event.clientY})
    }
    window.addEventListener('mousemove', handleWindowMouseMove)

    return () => {
      window.removeEventListener('mousemove', handleWindowMouseMove)
    }
  }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>vedfish</title>
        <meta name="description" content="vedfish generation" />
        <meta name="viewport" content="width=1024" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={[styles.vedtext, styles.vertical_t].join(' ')}>
        <div className={styles.scrolling}>
          <p>vedfish</p>
          <p>vedfish</p>
          <p>vedfish</p>
          <p>vedfish</p>
          <p>vedfish</p>
        </div>
      </div>
      <cursorContext.Provider value={cursor}>
      <main className={styles.main}>
        <div>
          <hr className={styles.tophr} />
        </div>
        <div className={styles.main_container}>
          <span className={styles.papers}>
            <Papers />      
            <Light />
          </span>
          <div>
            <hr style={{margin: 0, padding: 0}} />
          </div>
          <div className={styles.row2}>
            <div className={styles.boxy}>
              <Circles />
            </div>
            <div className={styles.road}>
              <Road />
            </div>
          </div>
          <div>
            <hr style={{margin: 0, padding: 0}} />
          </div>
          <div className={styles.birdbox}>
            <Birds />
          </div>
          <div>
            <hr style={{margin: 0, padding: 0}} />
          </div>
        </div>
      </main>
      </cursorContext.Provider>
    </div>
  )
}
